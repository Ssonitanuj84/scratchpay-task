# Overview

Solution is designed to be optimized for GCP, it uses Deployment Manager to deploy the the VM with Nginx and an HTTP server. GCP StackDriver Activity Log are filtered & sink(ed) to PUB/SUB which eventually triggers a Cloud Function to send notifications to Google Chat whenever a VM is ready.

Log Sink, Cloud PUB/SUB, Cloud Function are also provisioned/deployed with Deployment Manager to make sure we keep complete infrastructure as code in repositories.

To capture VM internal logs and telemetry details, StackDriver Monitoring Agent and StackDriver Logging Agent are also installed in the VM with proper permissions.

# Setup

## Pre-Requisites

To try out this solution first we need to perform the following tasks to get access to GCP project via gcloud cli. The user must be Project Owner of google cloud to setup the deployment manager permissions.

1. Open gcloud enabled terminal in which desired user account is logged in.

2. Set GCP project to your desired project using 
```gcloud config set project [YOUR-GCP-PROJECT-NAME]```

3. Open solution directory in that terminal and enable the required Google Service APIs by running 
```./enable-required-apis.sh```

4. Go to Cloud IAM and give Project/Owner permission to the `Google APIs Service Agent` account. The account can be identified by `[PROJECT-NUMBER]@cloudservices.gserviceaccount.com`, This can also be accomplished with gcloud cli running following commands:

```gcloud projects add-iam-policy-binding [PROJECT-ID] --member serviceAccount:[PROJECT-NUMBER]@cloudservices.gserviceaccount.com --role roles/owner```

```gcloud projects remove-iam-policy-binding [PROJECT-ID]--member serviceAccount:[PROJECT-NUMBER]@cloudservices.gserviceaccount.com --role roles/editor```

## Run Solution

`./deploy.sh "[DEPLOYMENT_NAME]" "[WEBHOOK_URL]"`

Note: WEBHOOK_URL is the Google Chat Room webhook-url and DEPLOYMENT_NAME is the name of the Deployment Manager deployment.

# Implementation Details

Above command `./deploy.sh "[DEPLOYMENT_NAME]" "[WEBHOOK_URL]"` does the following three things:-

1. Setting up Basic Infrastructure
2. Setting up a Notification Pipeline
3. Deploying the VM

## 1. Setting up Basic Infrastructure

This includes the provisioning of a Custom Service Account, Firewall Rule and Cloud Storage Bucket (For storing cloud function source code). This all happens with Deployment manager and needs to be done only once. This is all implemented in file `sa-bucket-firewall.jinja`

This step can individually be executed via this command:
```gcloud deployment-manager deployments create $DEPLOYMENT_NAME_SA --template sa-bucket-firewall.jinja```

Note: DEPLOYMENT_NAME_SA is the name of this Deployment Manager deployment which will be provided as an argument in the VM deployment step.

## 2. Setting up Notification Pipeline

In this step we create a log sink with filter and a topic on Cloud PUB/SUB. Then a Cloud Function is deployed which listens to messages (VM Ready Activity Logs) from PUB/SUB and then calls the Google Chat Webhook to send notifications for VM Status (Ready/Error) and Application Ready Status. Implementation is done in file: `sa-bucket-firewall.jinja`

This step can individually be executed via this command:
```gcloud deployment-manager deployments create $DEPLOYMENT_NAME_NOTIF_INFRA --template cloud-function.jinja --properties webhookUrl:$WEBHOOK_URL```

Note: DEPLOYMENT_NAME_NOTIF_INFRA is the name of this Deployment Manager deployment. WEBHOOK_URL is the webhook URL of Google Chat Room.

## 3. Deploying VM

A Shielded Virtual Machine is deployed which contains the application to echo HTTP request. Shielded VMs are a pre-hardened VM type provided by GCP. Using startup-script following things are done:-

- Download and execute the content of https://storage.googleapis.com/scratchpay-com-assets/challenges/hardening-dummy.sh for further hardening.
- Install nginx with a reverse proxy listening on port 3333 and rerouting locally on port 8080.
- Install node, pm2 and then start a node application server on port 8080 using pm2.
- Install stackdriver agent on the VM and configure it to log system, SSH log tentatives, all application servers.

Implementation is done in file: `scratch-financial-vm.jinja`

To run this step separately use the following command:

```gcloud deployment-manager deployments create $DEPLOYMENT_NAME_VM --template scratch-financial-vm.jinja --properties serviceAccountDeploymentName:$DEPLOYMENT_NAME_SA```

Note: DEPLOYMENT_NAME_VM is the name of this Deployment Manager deployment and also the provisioned VM. DEPLOYMENT_NAME_SA is the name of the service account deployment.

This step can be executed multiple time to deploy multiple VMs and the status of all those will be broadcasted to the Google Chat room using the same notification pipeline deployed in earlier steps.

# Deployment Failure Handling

Delete the storage bucket named "cf-bucket-[PROJECT-ID]" and then delete your Deployment Manager Deployment. Now create another deployment with a different name, `./deploy.sh "[DEPLOYMENT_NAME]" "[WEBHOOK_URL]"`

_Note: Do not re-create a deployment with same name as a previously deleted deployment. This is due to the fact that a service account with the same name as deployment name was already created earlier, this might cause unexpected behaviour e.g. stackdriver logging not working etc._


# Further Improvements
  1. Instead of Startup script we can use a prebuilt image packaging nginx, Application and other requirements.
  2. Use Managed instance group with a regional configuration for autoscalling and fault tollerance


## Required Google APIS

- Cloud Deployment Manager V2 API

  `https://console.cloud.google.com/marketplace/details/google/deploymentmanager.googleapis.com?project=[PROJECT-ID]`

- Compute Engine API

  `https://console.developers.google.com/apis/library/compute.googleapis.com?project=[PROJECT-ID]`

- Identity and Access Management (IAM) API

  `https://console.developers.google.com/apis/library/iam.googleapis.com?project=[PROJECT-ID]`

- Cloud Resource Manager API

  `https://console.developers.google.com/apis/library/cloudresourcemanager.googleapis.com?project=[PROJECT-ID]`

- Cloud Build API
  `https://console.developers.google.com/apis/library/cloudbuild.googleapis.com?project=[PROJECT-ID]`

- Cloud Functions API
  `https://console.developers.google.com/apis/library/cloudfunctions.googleapis.com?project=[PROJECT-ID]`