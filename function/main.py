from json import dumps
from httplib2 import Http
import base64
import ast
import os


def main(request):
    url = os.environ.get('webhookUrl', False)

    if url:
        request_json = request.get_json(silent=True)
        bot_message = {
            "text": "Something went wrong in Cloud Function, Please check the logs!"}

        if 'message' in request_json and 'data' in request_json['message']:
            request_json['message']['data'] = str(base64.b64decode(
                request_json['message']['data']))
            bot_message = {"text": str(request_json)}

        http_obj = Http()
        message_headers = {'Content-Type': 'application/json; charset=UTF-8'}
        response = str(http_obj.request(
            uri=url,
            method='POST',
            headers=message_headers,
            body=dumps(bot_message),
        ))

        if bot_message["text"] == "Something went wrong in Cloud Function, Please check the logs!":
            print(str({"Request Start": "==========================================",
                       "Request Json": request_json, "Request End": "=========================================="}))

        return f"Trigger Successful!"
    else:
        print(str({
            "Request Start": "==========================================",
            "Request Json": "Could not find Google Chat Webhook url!",
            "Request End": "=========================================="
        }))
        return f"Trigger Failed!"
