# ======= TEST DATA =======
# WEBHOOK_URL="https://chat.googleapis.com/v1/spaces/AAAA1E2j2cs/messages?key=AIzaSyDdI0hCZtE6vySjMm-WEfRq3CPzqKqqsHI&token=MOtRX97AZB03-VWB9grlb0B2drXoKFkTFSnlVsYQTPU%3D"
# DEPLOYMENT_NAME=my-deployment
# ======= END TEST DATA =======

export DEPLOYMENT_NAME=$1
export WEBHOOK_URL=$2
export DEPLOYMENT_NAME_SA=$DEPLOYMENT_NAME-sa-bkt-fw
export DEPLOYMENT_NAME_NOTIF_INFRA=$DEPLOYMENT_NAME-notif-infra
export DEPLOYMENT_NAME_VM=$DEPLOYMENT_NAME-vm

printf "\n==================================================\n\n"
printf "DEPLOYMENT_NAME: $DEPLOYMENT_NAME\n"
echo "WEBHOOK_URL: $WEBHOOK_URL"
printf "\n==================================================\n\n"

gcloud deployment-manager deployments create $DEPLOYMENT_NAME_SA --template sa-bucket-firewall.jinja

gcloud deployment-manager deployments create $DEPLOYMENT_NAME_NOTIF_INFRA --template cloud-function.jinja --properties webhookUrl:$WEBHOOK_URL

gcloud deployment-manager deployments create $DEPLOYMENT_NAME_VM --template scratch-financial-vm.jinja --properties serviceAccountDeploymentName:$DEPLOYMENT_NAME_SA
