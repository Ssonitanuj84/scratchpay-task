# Cloud Architect Challenge

Scratch Financial is a financial service company based in Pasadena, CA which provides a veterinary payment solutions for pet owners. It leverages data to assess risk, determine a credit rating and financing eligibility and assign appropriate interest rates when someone is approved.

As a Cloud Architect, you need to ensure that all the services can reach 99% SLA(uptime), everything is up-to-date and that we can reuse the services when we need to. In this challenge, please make a script to create a new service on either Google Cloud / AWS (Google Cloud prefered) with following criteria:

- Create the VM
- Install Nginx with a reverse proxy listening on port 3333 and rerouting locally on port 8080 and make a small application that just ECHOs HTTP request on port 8080 (in any language you want)
- Harden it using this script (or if you want to make your own on different OS configuration or a more updated one and we can use it at Scratch, we will compensate you for it.)
- Ensure that we use a custom service account and not the default one
- Make sure that everything is logged in Stackdriver including system, SSH log tentatives, all application servers
- Make sure that the firewall is well configured and no extra port is opened
- Publish the VM ready or error status to Google Chat
