
echo "============================================================"
echo "============== ENABLING REQUIRED API SERVICES =============="
gcloud services enable deploymentmanager.googleapis.com
gcloud services enable compute.googleapis.com
gcloud services enable iam.googleapis.com
gcloud services enable cloudresourcemanager.googleapis.com
gcloud services enable cloudbuild.googleapis.com
gcloud services enable cloudfunctions.googleapis.com
echo "============ DONE ENABLING REQUIRED API SERVICES ==========="
echo "============================================================"
